import config from "../config";
import axios from "axios";
function axios_api(api) {
  axios.defaults.baseURL = api;
  axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
  axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
  return axios
}

async function GetForms(id) {
  let json = []
        if(!id){
          return
        }
      const result = await axios_api(config.api).post(config.api+'/forms',{idformulario:id,FormBuilder:true}).then((res)=>{
        console.log('id, res.data :', id, res.data);
          return {'data':res.data,'ok':true};
        
        }).catch((error)=>{
          console.log('error:', error);
          return {'data':error,'ok':false}
        });
        if(result.data.message){
          return {'data':'el id del formulario no existe o el fomulario tiene errores','ok':false};
        }
        if(result.data.error){
          return {'data':result.data.solution,
                   error:result.data.error ,
                   field: result.data.field&& result.data.field,
                   'ok':false};
        }
        for (const key in result.data.form) {
            json.push(JSON.parse(result.data.form[key]));

        }
        console.log('result :', result);
        return {'data':json,'ok': result.ok,'entity':result.data.entity};
}



async function saveForm(body) {
  console.log('result:',body);
  const result = await axios_api(config.api+'/formulary').post(config.api+'/formulary',body).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}

async function validateForm(body) {
  const result = await axios_api(config.api+'/valideform').post(config.api+'/valideform',body).then((res)=>{
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}


 function saveMask(data,id,maskVersion) {
  let body = {
    fieldsMasked: data,
    idForm: id,
    maskVersion:maskVersion
  }
  // console.log('result:',body);
  const result =  axios_api(config.api+'/saveMask').post(config.api+'/saveMask',body).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}

 function GetAllFormulary() {
  const result =  axios_api(config.api+'/formulary').get(config.api+'/formulary').then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result; 
}

 function seachFormulary(param) {
  const result =  axios_api(config.api+'/seachFormularyById/'+param).get(config.api+'/seachFormularyById/'+param).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result; 
}

 function GetAllFormularyDataTable() {
  const result =  axios_api(config.api+'/GetFormularyDataTable').get(config.api+'/GetFormularyDataTable').then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result; 
}
 function GetLatestMaskVersion(idForm) {
  const result =  axios_api(config.api+'/GetLatestMaskVersion/' + idForm).get(config.api+'/GetLatestMaskVersion/' + idForm).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}
 function GetMaskVersionById(id) {
  const result =  axios_api(config.api+'/GetMaskVersionById/' + id).get(config.api+'/GetMaskVersionById/' + id).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}

 function GetAllMaskVersionsPerForm(idForm) {
  const result =  axios_api(config.api+'/GetAllMaskVersionsPerForm/'+ idForm).get(config.api+'/GetAllMaskVersionsPerForm/'+ idForm).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}

async function GetOneMaskVersionsPerForm(idForm) {
  const result = await axios_api(config.api+'/GetMaskVersionByIdForm/'+ idForm).get(config.api+'/GetMaskVersionByIdForm/'+ idForm).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}


async function sendForm(body) {
  console.log('data :', body);
  const result = await  axios_api(config.api+'/sendForms').post(config.api+'/sendForms',body).then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
  
}
 function saveRules(body) {
  console.log('data :', body)
  const result =  axios_api(config.api+'/rule').post(config.api+'/rule',body).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
  
}

 function GetRulesById(id) {
  const result =  axios_api(config.api+'/rule/'+id).get(config.api+'/rule/'+id).then((res)=>{
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false}
  });
  return result;
}

 function GetLastversionRule(id) {
  const result = axios_api(config.api+'/lastversion/'+id).get(config.api+'/lastversion/'+id).then((res)=>{    
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });
  return result;
}

 function GetRulesByForm(id) {
  const result =  axios_api(config.api+'/ruleByForm/'+id).get(config.api+'/ruleByForm/'+id).then((res)=>{        
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });

  return result;
  
}

 async function GetFormularyByServices(id) {
  const result = await axios_api(config.api+'/formularyByService/'+id).get(config.api+'/formularyByService/'+id).then((res)=>{        
    return {'data':res.data,'ok':true};
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });

  return result;
  
}
 async function GetFormularyBySE(id) {
  console.log(id)
  const result =  await axios_api(config.api+'/formularySE/'+id).get(config.api+'/formularySE/'+id).then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false};
  });
  console.log('result$$$ :', result);

  if(result.data.Formulary.length>0){
    return result;
  }else{
    return {'data':result.data,'ok':false}
  }
  
}

 function GetLastversionFormByService(id) {

  const result =  axios_api(config.api+'/lastversionform/'+id).get(config.api+'/lastversionform/'+id).then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });

  return result;
  
}

 function GetLastAllInstitutions() {

  const result =  axios_api(config.api+'/AllInstitutions').get(config.api+'/AllInstitutions').then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });

  return result;
  
}

 function GetServicesByInstituion(id) {

  const result =  axios_api(config.api+'/servicesByInstituion/'+id).get(config.api+'/servicesByInstituion/'+id).then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });

  return result;
  
}


 function GetServiceById(id) {

  const result =   axios_api(config.api+'/service/'+id).get(config.api+'/service/'+id).then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false};;
  });

  return result;
  
}

 function GetInstitutionById(id) {

  const result =  axios_api(config.api+'/institution/'+id).get(config.api+'/institution/'+id).then((res)=>{
        
    return {'data':res.data,'ok':true};
  
  }).catch((error)=>{
    return {'data':error,'ok':false};
  });

  return result;
  
}





export {
    GetForms,
    sendForm,
    GetAllFormulary,
    saveForm,
    saveMask,
    GetRulesById,
    GetFormularyBySE,
    GetRulesByForm,
    saveRules,
    GetLastversionRule,
    GetLastversionFormByService,
    GetLastAllInstitutions,
    GetServicesByInstituion,
    GetLatestMaskVersion, 
    GetAllMaskVersionsPerForm,
    GetServiceById,
    GetInstitutionById,
    GetMaskVersionById,
    GetAllFormularyDataTable,
    GetFormularyByServices,
    GetOneMaskVersionsPerForm,
    seachFormulary,
    validateForm
}