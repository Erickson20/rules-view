import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Route, } from "react-router-dom";
import AppRoutes from "./router";
import './index.css';
import * as serviceWorker from './serviceWorker';
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';

ReactDOM.render(
<Route>
    <AppRoutes/>
</Route>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
