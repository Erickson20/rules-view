import React, { Component } from 'react';
import { MDBCarousel, 
         MDBCarouselCaption, 
         MDBCarouselInner, 
         MDBCarouselItem, 
         MDBView, 
         MDBMask, 
         MDBCard,
         MDBCardTitle,
         MDBCardText,
         MDBContainer,
         MDBRow, 
         MDBCol,
         MDBIcon, } from "mdbreact";
         import config from '../../config';
class Home extends Component {
  render() {
    console.log('env :', config.env);
    return (
      <div className="bg">
      <MDBCarousel activeItem={1} length={4} showControls={true} showIndicators={true} className="z-depth-1">
        <MDBCarouselInner>
          <MDBCarouselItem itemId="1">
            <MDBView>
              <img className="d-block w-100" src={require('../../img/rules.jpg')} alt="First slide" />
            </MDBView>
            <MDBCarouselCaption>
              <h3 className="h3-responsive">Crea Reglas!</h3>
              <p>Dale vida a los formularios de SPL</p>
            </MDBCarouselCaption>
          </MDBCarouselItem>
          <MDBCarouselItem itemId="2">
            <MDBView>
              <img className="d-block w-100" src={require('../../img/mask.jpg')}  alt="Second slide" />
            </MDBView>
            <MDBCarouselCaption>
              <h3 className="h3-responsive">Asigna Mascaras</h3>
              <p>Evita que el usuario digite errores</p>
            </MDBCarouselCaption>
          </MDBCarouselItem>
          <MDBCarouselItem itemId="3">
            <MDBView>
              <img className="d-block w-100" src={require('../../img/email.jpg')}  alt="Third slide" />
            </MDBView>
            <MDBCarouselCaption>
              <h3 className="h3-responsive">Crea Correos</h3>
              <p>La forma mas facil de crear una plantilla de correo</p>
            </MDBCarouselCaption>
          </MDBCarouselItem>
          <MDBCarouselItem itemId="4">
            <MDBView>
              <img className="d-block w-100" src={require('../../img/cert.jpg')} alt="Mattonit's item" />
            </MDBView>
            <MDBCarouselCaption>
              <h3 className="h3-responsive">Configura y Genera Certificaciones</h3>
              <p>genera documentos para ser enviado a softexpert</p>
            </MDBCarouselCaption>
          </MDBCarouselItem>
        </MDBCarouselInner>
      </MDBCarousel>
    </div>
    );
  }
}

export default Home;