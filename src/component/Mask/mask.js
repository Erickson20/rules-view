import React, { Component } from 'react';
import { MDBRow,MDBBtn, MDBCol,MDBIcon,MDBCard,MDBProgress, MDBBadge , MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter} from "mdbreact";
import { TDate,TText,TTime } from "../form/index";
import {GetForms, saveMask,  GetFormularyBySE, GetLatestMaskVersion} from "../../data/api";
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {ResumenCardMask} from'./resumenCardMask';
import AllFormulary from '../formulary/allFormulary';
class Mask extends Component {
  constructor(props){
    super(props)
    this.state = {
      modal: false,
      addToResumen: [],
      arrayWithMasks: [],
      ToInput: '',
      idForm: '',
      getUnique:'',
      active: false,
      mask: 0,
      data: [] ,
      name:'',
      actualName:'',
      count:0,
      actualTextValue:'',
      oldValueMask:'empty',
      progress: 0,
      message: false,
      arrayMask: [], 
      removePrevious:'',
      arrayMaskShow:[],
      setId:[],
      label:'',
      textValue:''
    }
    this.addToResumen = this.addToResumen.bind(this);
    this.ToInput = this.ToInput.bind(this)
    this.searchForm = this.searchForm.bind(this);
    this.idForm = this.idForm.bind(this);
    this.mask = this.mask.bind(this);
    this.removePrevious = this.removePrevious.bind(this);

  }

  async ToInput(textValue,id,name, oldValueMask, active,label){
    this.state.setId = await GetFormularyBySE(this.state.idForm)
    let selectAdd = document.getElementById('Add');
    selectAdd.removeAttribute("disabled",false);
    this.state.active = active;
    if(this.state.active===false){
    this.setState({
      ToInput: id,
    })
    let selectDisable = document.getElementsByTagName('select');
    selectDisable[0].setAttribute("disabled",true);
    //console.log(this.state.data[0]);
    let element = document.getElementById(name)
    let count = this.state.count;
    this.setState({name:name})
    this.state.data[0].forEach(function(value){
      if(value['name']===name && count < 1 ){
        toast.success(`Opción pre-guardada`,  {
          position: toast.POSITION.BOTTOM_CENTER
        })
        value['Mask']= parseInt(textValue);
        element.setAttribute("style", "display:block; border-style: dotted;border-width: 3px; border-color:#16B9B3; background-color:lightgreen;");
      }
    });
    this.setState({
      label:label,
      textValue:textValue,
      actualName: name,
      actualTextValue: textValue,
      oldValueMask: oldValueMask
    })
    this.state.active = true;
    }
    console.log(this.state.arrayMaskShow,'verifying')
   } 
   async searchForm(e){
    this.setState({
      progress: 60
    })
    const data = await GetForms(this.state.idForm)
    console.log(data, 'here')
    // console.log(data);
      if(data.ok){
      this.setState({
        progress: 80
      })
      this.setState({ data: await this.fields(data.data)}) 
      this.setState({
        progress: 100
      })
     }else{
      this.setState({
        progress: 0,
        message: true
      })
     }
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }
  idForm(e){
    this.setState({
      idForm: e.target.value
    })
  }
  mask(e){
    this.setState({
        mask: e.target.value,
    })
  }
  fields(data){
    let SelectedAndRadios = []
    let field = [];
    for (const i in data) {
        for (const key in data[i]) {
            if(data[i][key].type ==='radio-group' || data[i][key].type === 'select' ){
                SelectedAndRadios.push(data[i][key])
            }
            field.push(data[i][key])
        }
     }
     return [field,SelectedAndRadios]
  }
  refresh(){
    let name =  this.state.name;
    let count = this.state.count;
    let element = document.getElementById(name)
    if(element !==null ){
      element.setAttribute("style", "")
      let oldValue = this.state.oldValueMask;
      console.log(oldValue, name)
      this.state.data[0].forEach(function(value){
        if(value['name'] === name && count !== 1)
              value['Mask'] = oldValue;
      });
    }
    //console.log(this.state.data[0]);
    this.setState({
      count: 0,
      })
      let selectAdd = document.getElementById('Add');
      selectAdd.setAttribute("disabled",true);
      let selectDisable = document.getElementsByTagName('select');
      selectDisable[0].removeAttribute("disabled",false);
      this.setState({
        active:false
      })
      // this.state.active = false;
  }
  addToResumen(label,textValue,name){
    label = this.state.label
    textValue = this.state.textValue
    name = this.state.actualName
  
    let textarea = document.getElementById("resumenArea");
    this.state.arrayMask.push('D'+textValue+'D'+name)
    this.state.arrayMaskShow.push('Mask' + textValue +  '=>' + [label.replace(':','')] +'\n')
    console.log('from resumen', this.state.arrayMaskShow)
    console.log('checking config' ,this.state.arrayMask)
    textarea.value= this.state.arrayMaskShow;
    for(const valores in this.state.arrayMask){
      if(textarea.value==='')
        textarea.value += valores 
    }
    this.refresh()
  }
  removePrevious() {
    toast.success(`Opciones borradas`,  {
      position: toast.POSITION.BOTTOM_CENTER
    })
    let insideText = document.getElementById('resumenArea');
    try{
      if (this.state.arrayMask.length > 0 &&  this.state.arrayMaskShow) {
        this.state.arrayMaskShow.pop()
        this.state.arrayMask.pop()
        insideText.value = ''
        for(let i =0 ; i < this.state.arrayMaskShow.length; i++){
          insideText.value +=  this.state.arrayMaskShow[i]
        }
        console.log(this.state.arrayMaskShow, 'removing1');
        console.log(this.state.arrayMask, 'removing2');
        
        // for(let i =0 ; i < this.state.arrayMaskShow.length; i++){
        //   if(this.state.arrayMaskShow[i] ==="" ){
        //     this.state.arrayMaskShow.pop()
        //   }
        // }
        
        // insideText.value = this.state.arrayMaskShow
        // console.log(insideText.value)
         this.refresh();
      } else {
        toast.error('El Campo ya esta vacio', {
          position: toast.POSITION.BOTTOM_CENTER
        })
      }
    }catch(e){
      console.log(e)
      toast.error('El Campo ya esta vacio', {
        position: toast.POSITION.BOTTOM_CENTER
      })
    }
    // console.log(this.state.arrayMask);
  }  
  componentWillReceiveProps(props){
    this.updateState(props)
  }
  updateState(newState){
    this.setState({
      date: newState
    })
  }
  /****************---- Enviar Configuracion  ----******************/
  async sendForm(e){
    try{
      if(this.state.arrayMaskShow.length > 0){
      this.toggle()
      let valor = await GetLatestMaskVersion(this.state.setId.data.Formulary[0]['id']);
      let newVersion = valor.data['mask']+1
      let finalArrayMask
      //console.log(newVersion + 'vuey')
      this.state.arrayMask.forEach(function(value){
        if(value !== undefined)
          finalArrayMask+= value
      })
      finalArrayMask = finalArrayMask.substring(9,finalArrayMask.length)
      console.log(finalArrayMask)
      //  const data = await saveMask(
      //   this.state.arrayMask[0], this.state.setId.data.Formulary[0]['id'], newVersion
      //   )
        toast.success(`Se han añadido las máscaras correctamente.`,  {
          position: toast.POSITION.BOTTOM_CENTER
      });
      toast.success(`Version No.${newVersion}`,  {
        position: toast.POSITION.TOP_RIGHT
      })
        } else{
          toast.error('No se ha asignado ninguna mascara aún', {
            position: toast.POSITION.BOTTOM_CENTER
        })
      }
    }catch(e){
        this.toggle()
        toast.error('Error en la conexión con el servidor', {
          position: toast.POSITION.BOTTOM_CENTER
      });
    }
  }
  render() {
    return (
       this.state.data.length>0? (
        <MDBRow>
        {/* <ToastContainer/> */}
        <MDBCol size="3"  className="border">
          <label>Seleccionar Mascara</label>
            <select className="form-control"  onChange={this.mask} value={this.state.mask}>
            <option value="0"> 0 - CEDULA</option><option value="1"> 1 - RNC</option><option value="2"> 2 - TELÉFONO</option>
                <option value="3"> 3 - CELULAR</option><option value="4"> 4 - CODIA</option><option value="5"> 5 - EMAIL</option>
                <option value="6"> 6 - SOLO NUMERO</option><option value="7"> 7- SOLO LETRA</option><option value="8"> 8 - INMUEBLE</option>
                <option value="9"> 9 - COD.SISTEMA CAASD</option><option value="10"> 10 - FECHA</option><option value="11">11 - HORA</option>
                <option value="12">12 - DECIMAL</option><option value="13">13 - EXPEDIENTE CAID</option><option value="14">14 - NSS INABIMA</option>
                <option value="15">15 - CARGA EDESUR</option><option value="16"> 16 - MONTO INABIMA</option><option value="17">17 - LATITUD</option>
                <option value="18"> 18 - LONGITUD</option><option value="19">19 - MAYUSCULAS</option><option value="20">20 - CÉDULA INABIMA</option>
                <option value="21">21 - EXPEDIENTE CAID</option><option value="22">22 - FECHA NO MAYOR ACTUAL</option><option value="23">23 - FECHA NO MENOR ACTUAL</option>
                <option value="24">24 - EDAD NO MAYOR A LA ENVIADA</option><option value="27"> 27- Monto general No negativo</option>
                <option value="28"> 28 - Entero no negativo</option><option value="29">29 - fecha de vuelo menor a 48 horas</option>
                <option value="30">30 - fecha de vuelo menor a 48 horas</option><option value="31">31 - fecha de embarque</option>
                <option value="32">32 - validacion de maestro y solicitud abierta</option>
                {/* 29 - fecha de vuel menor a 48 horas
                  30 - fecha de vuelo menor a 48 horas
                  31 - fecha de embarque
                  32 - validacion de maestro y solicitud abierta */}
            </select>
            <MDBBtn color="default" onClick={() => this.refresh(this.state.mask , this.state.actualTextValue , this.state.actualName)}>SELECCIONAR</MDBBtn>
            <MDBBtn color="default" id="Add"  onClick={() =>this.addToResumen(this.state.actualTextValue, this.state.actualName)}>Agregar</MDBBtn>
            <MDBBtn color="default" id="Add"  onClick={() =>this.removePrevious()}>Remover</MDBBtn>
            <MDBRow className="marginSelected">
                <MDBCol size="12" className="cursor">
                <ResumenCardMask  />
                <MDBBtn color="default" onClick={() =>this.toggle()}>Guardar Configuracion</MDBBtn>
                </MDBCol>
          </MDBRow>
          </MDBCol>
          <MDBCol size="9" >
          <MDBRow className="marginSelected">
          {  
              this.state.data[0].map((field, i) =>
              field.type === 'text' ? (
                <MDBCol size="3" className="cursor" onClick={() => this.ToInput(this.state.mask, i ,field.name, field.Mask, this.state.active, field.label)}>
                    <MDBBadge> Mascara actual: {
                        field.Mask === 0? 'Cédula': field.Mask === "0"? 'Cédula': field.Mask == 1? 'RNC': field.Mask == 2? 'Teléfono':
                        field.Mask == 3? 'Celular': field.Mask == 4? 'Codia': field.Mask == 5? 'Email': field.Mask == 6? 'Solo Número': field.Mask == 7? 'Solo Letra': field.Mask == 8? 'Inmueble':
                        field.Mask == 9? 'COD.SISTEMA CAASD': field.Mask == 10? 'Fecha': field.Mask == 11? 'Hora': field.Mask == 12? 'Decimal' : field.Mask == 13? 'Expediente CAID' : field.Mask == 14? 'NSS INABIMA':
                        field.Mask == 15? 'CARGA EDESUR': field.Mask == 16? 'MONTO INABIMA': field.Mask == 17? 'LATITUD': field.Mask == 18? 'LONGITUD': field.Mask == 19? 'MAYÚSCULAS': field.Mask == 20? 'CEDULA INABIMA':
                        field.Mask == 21? 'EXPEDIENTE CAID': field.Mask == 22? 'FECHA NO MAYOR ACTUAL': field.Mask == 23? 'FECHA NO MENOR ACTUAL': field.Mask == 24? 'EDAD NO MAYOR A LA ENVIADA': field.Mask == 25? ' ': field.Mask == 26? ' ':
                        field.Mask == 27? 'Monto general No negativo': field.Mask == 28? 'Entero no negativo':  field.Mask == 29? 'Fecha de vuelo menor a 48 horas':  field.Mask == 30? 'Fecha de vuelo menor a 48 horas' :
                        field.Mask == 31 ? 'fecha de embarque' : field.Mask == 32? 'Validacion de maestro y solicitud abierta': 'Ninguna'
                      }
                    </MDBBadge>
                    <TText  key={i} oldValue={field.mask} id={i + field.name} field={field} />
                </MDBCol>
                ):
                 
                field.type === 'time' ? (
                    <MDBCol size="3" className="cursor"  onClick={(e) => this.ToInput(this.state.mask, i ,field.name, field.Mask, this.state.active, field.label)}>
                      <MDBBadge> Mascara actual: {
                        field.Mask === 0? 'Cédula': field.Mask === "0"? 'Cédula': field.Mask == 1? 'RNC': field.Mask == 2? 'Teléfono':
                        field.Mask == 3? 'Celular': field.Mask == 4? 'Codia': field.Mask == 5? 'Email': field.Mask == 6? 'Solo Número': field.Mask == 7? 'Solo Letra': field.Mask == 8? 'Inmueble':
                        field.Mask == 9? 'COD.SISTEMA CAASD': field.Mask == 10? 'Fecha': field.Mask == 11? 'Hora': field.Mask == 12? 'Decimal' : field.Mask == 13? 'Expediente CAID' : field.Mask == 14? 'NSS INABIMA':
                        field.Mask == 15? 'CARGA EDESUR': field.Mask == 16? 'MONTO INABIMA': field.Mask == 17? 'LATITUD': field.Mask == 18? 'LONGITUD': field.Mask == 19? 'MAYÚSCULAS': field.Mask == 20? 'CEDULA INABIMA':
                        field.Mask == 21? 'EXPEDIENTE CAID': field.Mask == 22? 'FECHA NO MAYOR ACTUAL': field.Mask == 23? 'FECHA NO MENOR ACTUAL': field.Mask == 24? 'EDAD NO MAYOR A LA ENVIADA': field.Mask == 25? ' ': field.Mask == 26? ' ':
                        field.Mask == 27? 'Monto general No negativo': field.Mask == 28? 'Entero no negativo':  field.Mask == 29? 'Fecha de vuelo menor a 48 horas':  field.Mask == 30? 'Fecha de vuelo menor a 48 horas' :
                        field.Mask == 31 ? 'fecha de embarque' : field.Mask == 32? 'Validacion de maestro y solicitud abierta': 'Ninguna'
                      }
                      </MDBBadge>
                        <TTime key={i} oldValue={field.mask}  id={i + field.label} field={field}/>
                    </MDBCol>
                )
                :
                field.type === 'date' && (
                    <MDBCol size="3" className="cursor disable-fields"  onClick={(e) => this.ToInput(this.state.mask, i ,field.name, field.Mask, this.state.active, field.label)}>
                      <MDBBadge> Mascara actual: {
                        field.Mask === 0? 'Cédula': field.Mask === "0"? 'Cédula': field.Mask == 1? 'RNC': field.Mask == 2? 'Teléfono':
                        field.Mask == 3? 'Celular': field.Mask == 4? 'Codia': field.Mask == 5? 'Email': field.Mask == 6? 'Solo Número': field.Mask == 7? 'Solo Letra': field.Mask == 8? 'Inmueble':
                        field.Mask == 9? 'COD.SISTEMA CAASD': field.Mask == 10? 'Fecha': field.Mask == 11? 'Hora': field.Mask == 12? 'Decimal' : field.Mask == 13? 'Expediente CAID' : field.Mask == 14? 'NSS INABIMA':
                        field.Mask == 15? 'CARGA EDESUR': field.Mask == 16? 'MONTO INABIMA': field.Mask == 17? 'LATITUD': field.Mask == 18? 'LONGITUD': field.Mask == 19? 'MAYÚSCULAS': field.Mask == 20? 'CEDULA INABIMA':
                        field.Mask == 21? 'EXPEDIENTE CAID': field.Mask == 22? 'FECHA NO MAYOR ACTUAL': field.Mask == 23? 'FECHA NO MENOR ACTUAL': field.Mask == 24? 'EDAD NO MAYOR A LA ENVIADA': field.Mask == 25? ' ': field.Mask == 26? ' ':
                        field.Mask == 27? 'Monto general No negativo': field.Mask == 28? 'Entero no negativo':  field.Mask == 29? 'Fecha de vuelo menor a 48 horas':  field.Mask == 30? 'Fecha de vuelo menor a 48 horas' :
                        field.Mask == 31 ? 'fecha de embarque' : field.Mask == 32? 'Validacion de maestro y solicitud abierta': 'Ninguna'
                      }
                      </MDBBadge>
                        <TDate key={i} oldValue={field.mask} id={i + field.label} field={field}/>
                    </MDBCol>
                )
              )
          }
          </MDBRow>
          </MDBCol>
          <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="lg" >
                  <MDBModalHeader toggle={this.toggle}>
                    <center>Guardar Máscaras</center>
                  </MDBModalHeader>
                  <MDBModalBody>
                  <center> ¿Desea guardar las Máscaras Añadidas?</center>
                  <div>
                    <center>
                    <b>{this.state.arrayMaskShow}</b>
                    </center>
                  </div>
                  </MDBModalBody>
                  <MDBModalFooter>
                  <MDBBtn  color="primary"  onClick={()=> this.toggle()}>Cancelar</MDBBtn>
                  <MDBBtn color="success" onClick={()=> this.sendForm()}>Guardar</MDBBtn>
                  </MDBModalFooter>
            </MDBModal>
        </MDBRow>
       ):
        (
          <div>
            <br/><br/><br/><br/><br/><br/><br/>
            <h1>Buscar Formulario por id</h1>
            <MDBRow>
            <MDBCol size="3"></MDBCol>
            <MDBCol size="6">
            <MDBCard className="default-color padding">
            <input placeholder="Ej: SIV01FMSOLSERV"  className="form-control"onChange={ this.idForm } value={ this.state.idForm }/>
            <button  className="btn btn-lg text-white" style={{backgroundColor: '#00695c'}} onClick={ this.searchForm }>Buscar formulario</button>
            </MDBCard>
              <MDBProgress value={this.state.progress} color="default" className="my-1 text-white white"  />
            {
              this.state.message && (
                <h5 className="text-danger"><b>El id del formulario no existe</b></h5>
              )
            }
            </MDBCol>
            <MDBCol size="3"></MDBCol>
            </MDBRow>
          </div>
       )
    );
  } 
}
export default Mask;