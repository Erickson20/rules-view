import React, { Component } from 'react';
import Types from "prop-types";
import './App.css';
import  Content  from "../Body/content";
import Header from "../Body/header";
import { ToastContainer } from 'react-toastify';

class App extends Component {
  static Types = {
    children: Types.object.isRequired
  }
  render() {
    const {children} = this.props
    return (
      <div className="App">
        <Header />
        <ToastContainer/>
        <Content body={children}/>
      </div>
    );
  }
}

export default App;
