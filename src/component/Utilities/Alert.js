import React, { Component } from 'react';
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Page404 extends Component {

    constructor(props){
        super(props)
    }

    componentWillReceiveProps(props){
        if(this.props != props){
        console.log('props :', props);
           toast[props.type](props.message, {
            position: toast.POSITION[props.position]
          });
        }

    }

     shouldComponentUpdate(nextProps, nextState) {
        return false
     }

  
  render() {
    return (
      <div className="App">
       {/* <ToastContainer/> */}
      </div>
    );
  }
}

export default Page404;