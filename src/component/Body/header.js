import React, { Component } from "react";
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
MDBIcon, MDBDropdownItem, MDBDropdownMenu, MDBDropdownToggle,  MDBContainer,MDBNav,
MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from "mdbreact";
import './style.css';
import { firebase } from "../../firebase";
import { ErrorByFirebase } from "../../Helpers/helpers";
import { ToastContainer, toast } from 'react-toastify';
import {  Redirect } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
class Header extends Component {
constructor(props){
  super(props)
  this.state = {
    isOpen: false,
    user: '',
    pass: '',
    loading: false,
    modal: false,
    modalRegister: false,
    isLogin: false,
    nameUser: '',
    activeItem: "1"
  };

  this.UserInput = this.UserInput.bind(this);
  this.PassInput = this.PassInput.bind(this);

}
toggleCollapse = () => {
  this.setState({ isOpen: !this.state.isOpen });
}
toggle = () => {
 this.setState({
    modal: !this.state.modal
  });
}
toggleTab = tab => () =>{
  if (this.state.activeItem !== tab) {
    this.setState({
      activeItem: tab
    })
  }
}

toggleRegister = () => {
  this.setState({
    modalRegister: !this.state.modalRegister
  });
}
UserInput(e){
  this.setState({
    user: e.target.value
  })
}
PassInput(e){
  this.setState({
    pass: e.target.value
  })
}
async CreateUser(){
  this.setState({
    loading: true
  })
  const result = await firebase.auth()
  .createUserWithEmailAndPassword(this.state.user+'@rules.com', this.state.pass)
  .then((u)=>{
      var user = firebase.auth().currentUser;
      user.updateProfile({
        displayName: '2'
      }).catch((error) => {
        console.log('error al insertar rol :', error);
        
      });
      return  {'data': 'Usaurio registrado exitosamente', 'ok': true}
  })
  .catch((error)=> {
    console.log('error.message :', error.code);
    return {'data': error.code, 'ok': false}
  });

  if(result.ok){
    toast.success(result.data,{
      position: toast.POSITION.BOTTOM_CENTER
    })
    this.setState({
      loading: false,
      modalRegister: false
    })
  }else{
    toast.error(ErrorByFirebase(result.data),{
      position: toast.POSITION.BOTTOM_CENTER
    })
  }
  this.setState({
    loading: false,
  })

  console.log('result :', result);
}

async Login(){
  this.setState({
    loading: true
  })
  const result = await firebase.auth()
  .signInWithEmailAndPassword(this.state.user+'@rules.com', this.state.pass)
  .then((user)=>{
    return {'data': firebase.auth().currentUser, 'ok': true}
  })
  .catch((error)=> {
    console.log('error :', error.message);
    return {'data': error.code, 'ok': false}
  });

  if(result.ok){
    toast.info('Bienvenido '+result.data.email.split('@')[0],{
      position: toast.POSITION.TOP_RIGHT
    })
    this.setState({
      modal: false,
      isLogin: true,
      nameUser: result.data.email.split('@')[0]
    })
  
  }else{
    console.log('result.data :', result.data);
    toast.error(ErrorByFirebase(result.data),{
      position: toast.POSITION.BOTTOM_CENTER
    })
  }
  this.setState({
    loading: false,
  })

}
logout(){
  firebase.auth().signOut().
  then((user)=>{
    this.setState({
      isLogin: false,
      nameUser:'',
      redirect: true
    })
    window.location.href = "/home";
  })
  .catch((error)=> {
    console.log('error :', error);
    toast.error('Ups! paso algo al salir de la cuenta',{
      position: toast.POSITION.BOTTOM_CENTER
    })
  });

}
componentDidMount(){
  
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      this.setState({
        isLogin: true,
        nameUser: user.email.split('@')[0]
      })
    }
  });
}
render() {
  return (
    <div>
      {/* <ToastContainer/> */}
    <MDBNavbar color="default-color" dark expand="md">
      <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
      {this.state.isLogin&& (
        <MDBNavbarNav left >
          <MDBNavItem >   
            <MDBNavLink to="/" className={this.state.activeItem==="1" ? "border-bottom border-white" : "" } onClick={this.toggleTab("1")} role="tab"> <MDBIcon icon="home" ></MDBIcon> Inicio</MDBNavLink>
          </MDBNavItem> 
            <MDBNavItem >  
              <MDBNavLink to="/Rules" className={this.state.activeItem==="2" ? "border-bottom border-white" : "" } onClick={this.toggleTab("2")} role="tab"> <MDBIcon icon="align-left" ></MDBIcon> Reglas</MDBNavLink>
            </MDBNavItem>  
            <MDBNavItem >           
              <MDBNavLink to="/Mask" className={this.state.activeItem==="3" ? "border-bottom border-white" : "" } onClick={this.toggleTab("3")} role="tab"> <MDBIcon icon="asterisk" ></MDBIcon> Mascaras</MDBNavLink>
            </MDBNavItem>  
            {/* <MDBNavItem >           
              <MDBNavLink to="/Mails" className={this.state.activeItem==="4" ? "border-bottom border-white" : "" } onClick={this.toggleTab("4")} role="tab"><MDBIcon icon="envelope" ></MDBIcon> Correos</MDBNavLink>
            </MDBNavItem> */}
            <MDBNavItem >           
              <MDBNavLink to="/Formulary" className={this.state.activeItem==="5" ? "border-bottom border-white" : "" } onClick={this.toggleTab("5")} role="tab"> <MDBIcon icon="file" ></MDBIcon> Formularios</MDBNavLink>
            </MDBNavItem>   
        </MDBNavbarNav>
        )}   
        <MDBNavbarNav right>
          <MDBNavItem>
            <MDBDropdown>
              <MDBDropdownToggle nav caret>
              {this.state.nameUser} 
                <MDBIcon icon="user" />
              </MDBDropdownToggle>
              <MDBDropdownMenu className="dropdown-default" right>
              {this.state.isLogin?(
                <MDBDropdownItem   onClick={()=>this.logout()}>Salir</MDBDropdownItem>
              ):(
                <div>
                <MDBDropdownItem  onClick={this.toggle}>Login</MDBDropdownItem>
                <MDBDropdownItem   onClick={this.toggleRegister}>Register</MDBDropdownItem>
                </div>
                )}
              </MDBDropdownMenu>
            </MDBDropdown>
          </MDBNavItem>
        </MDBNavbarNav>
      </MDBCollapse>
    </MDBNavbar>


      
      {/**====================================MODAL LOGIN=================================== */}
      <MDBContainer>
        
      <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="md" >
          <MDBModalHeader toggle={this.toggle}>Formulario de Inicio de Sesion</MDBModalHeader>
          <MDBModalBody>
          <p className="h4 text-center mb-4">Iniciar Sesion</p>
                      <label htmlFor="defaultFormLoginEmailEx" >
                      Usuario
                      </label>
                      <input
                      type="text"
                      id="id_formulary"
                      name="id_formulary"
                      className="form-control"
                      onChange={ this.UserInput}
                      value={ this.state.user}
                      />
                      <br/>
                       <label htmlFor="defaultFormLoginEmailEx" >
                     Contraseña
                      </label>
                      <input
                      type="password"
                      id="id_fowrmulary"
                      name="id_fowrmulary"
                      className="form-control"
                      onChange={this.PassInput}
                      value={ this.state.pass}
                      />
                      
                     
                      
                      <br />
          </MDBModalBody>
          <MDBModalFooter>
          {this.state.loading&&(
                          <MDBIcon icon="cog" spin size="3x" style={{color: '#00695c'}} fixed />
                      )}
          <MDBBtn color="default" size="lg" disabled={this.state.user ==='' || this.state.pass ===''? true: false} onClick={()=> this.Login()}>Entrar</MDBBtn>
          </MDBModalFooter>
      </MDBModal>
      </MDBContainer>




       {/**====================================MODAL REGISTER=================================== */}
       <MDBContainer>
        
        <MDBModal isOpen={this.state.modalRegister} toggle={this.toggleRegister} size="md" >
            <MDBModalHeader toggle={this.toggleRegister}>Formulario de Registro</MDBModalHeader>
            <MDBModalBody>
            <p className="h4 text-center mb-4">Registro</p>
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Usuario
                        </label>
                        <input
                        type="text"
                        id="id_formulary"
                        name="id_formulary"
                        className="form-control"
                        onChange={ this.UserInput}
                        value={ this.state.user}
                        />
                        <br/>
                        
                        <label htmlFor="defaultFormLoginEmailEx" >
                       Contraseña
                        </label>
                        <input
                        type="password"
                        id="id_fowrmulary"
                        name="id_fowrmulary"
                        className="form-control"
                        onChange={this.PassInput}
                        value={ this.state.pass}
                        />

                        
                        <br />
            </MDBModalBody>
            <MDBModalFooter>
            {this.state.loading&&(
                          <MDBIcon icon="cog" spin size="3x" style={{color: '#00695c'}} fixed />
                      )}
            <MDBBtn color="danger" size="lg" disabled={this.state.user ==='' || this.state.pass ===''? true: false} onClick={()=> this.CreateUser()}>Registrar</MDBBtn>
            </MDBModalFooter>
        </MDBModal>
        </MDBContainer>
      </div>
    );
  }
}

export default Header;