import React, { Component } from 'react';
import Types from "prop-types";
import IdleTimer from 'react-idle-timer'
import { firebase } from "../../firebase";
import { ToastContainer, toast } from 'react-toastify';
import {  Redirect } from 'react-router-dom';

import 'react-toastify/dist/ReactToastify.css';
class Page404 extends Component {
  constructor(props) {
    super(props)
    this.idleTimer = null
    this.onIdle = this._onIdle.bind(this)
    this.state = {
      isLogin: true
    }
  }
    static Types = {
        body: Types.object.isRequired
      }

      componentWillReceiveProps(){
        firebase.auth().onAuthStateChanged((user) => {
          if (user) {
            this.setState({
              isLogin: true,
            })
          }else{
            this.setState({
              isLogin: false,
            })
          }
        });
      }
  render() {
      const {body} = this.props
    return (
      <div className="App">
      {/* <ToastContainer/> */}
      <IdleTimer
      ref={ref => { this.idleTimer = ref }}
      element={document}
      onIdle={this.onIdle}
      debounce={250}
      timeout={3.6e+6} />
      {this.state.isLogin&& body}
     
      </div>
    );
  }

  logout(){
    firebase.auth().signOut().
    then((user)=>{
      toast.info('Caduco la session de usuario',{
        position: toast.POSITION.BOTTOM_CENTER
      })
      window.location.href = "/home";

    })
    .catch((error)=> {
      console.log('error :', error);
      toast.error('Ups! paso algo al salir de la cuenta',{
        position: toast.POSITION.BOTTOM_CENTER
      })
    });
  
  }
 
  _onIdle(e) {
    this.logout()
   
    console.log('cerro sesion')
  }
}

export default Page404;