import React, { Component } from 'react';
import {  MDBCol ,MDBRow} from "mdbreact";
export default class TText extends Component {
    constructor(props){
      super(props)
  
      
    }
  
    componentWillReceiveProps(props){
    }
    render() {
      const field = this.props.field
      const i = this.props.id
      return (
          <div className="form-group" >
          <label><b>{field.label}</b> <span id={'rule'+field.name}></span></label>
          <input
            type="text"
            className="form-control"
            id="formGroupExampleInput"
            key={i}
            name={field.name}
            oldMask={field.Mask}
            id={field.name}
            placeholder={'id: '+field.name}
            disabled="disabled"
          />
        </div>
      );
    }
  }
  
  