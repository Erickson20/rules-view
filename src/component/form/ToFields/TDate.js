import React, { Component } from 'react';
import {  MDBCol ,MDBRow} from "mdbreact";
export default class TDate extends Component {
    constructor(props){
      super(props)
 
    }
    componentWillReceiveProps(props){
    }
    render() {
      const field = this.props.field
      const i = this.props.id
      return (
          <div className="form-group" >
          <label htmlFor="formGroupExampleInput"><b>{field.label}</b></label>
          <input
            type="date"
            className="form-control"
            id="formGroupExampleInput"
            key={i}
            name={field.name}
            id={field.name}
            disabled="disabled"
          />
        </div>
      );
    }
  }
  
  