import React, { Component } from 'react';
import CardText from "../../Utilities/CardText";
import {  MDBCol ,MDBRow} from "mdbreact";
export default class THeader extends Component {
    constructor(props){
      super(props)
    }
  
    componentWillReceiveProps(props){
      
      
      
    }
    render() {
      const field = this.props.field
      const i = this.props.id
      return (
          <div className="form-group" >
          {field.subtype === 'h1' ? (
            <CardText key={i} id={field.name}  fs={false} color="#00695c" text={field.label}/>
          )
          :
          (
            <CardText key={i} id={field.name} fs={true}color="#bdbdbd " text={field.label }/>
          )
        
        
        }
            
           
        </div>
      );
    }
  }