import React, { Component } from 'react';
import {  MDBCol ,MDBRow} from "mdbreact";
export default class TTextarea extends Component {
    constructor(props){
      super(props)
  
      
    }
  
    componentWillReceiveProps(props){
      
      
    }
    render() {
      const field = this.props.field
      const i = this.props.id
      return (
          <div className="form-group" >
          <label htmlFor="formGroupExampleInput">{field.label}</label>
          <textarea
            className="form-control"
            key={i}
            name={field.name}
            id={field.name}
            disabled="disabled"
          >
          </textarea>
        </div>
      );
    }
  }
  
  