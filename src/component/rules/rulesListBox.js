import React, { Component } from 'react';
import { MDBRow, MDBCol,MDBIcon,MDBCard,MDBContainer,MDBModal,MDBModalHeader,MDBModalBody  } from "mdbreact";
import CardText from '../Utilities/CardText'; 
import {  Redirect } from 'react-router-dom';
import { GetRulesById  } from "../../data/api";

class RulesListBox extends Component {
    constructor(props){
        super(props)
        this.state = {
            shadow: '',
            newRule: false,
            rules:'',
            modal: ''
        }
    }
    async  viewRule(){
      const result = await GetRulesById(this.props.rules.id)
        this.setState({
          modal: true,
          rules: result.data.rules[0].rule
        })
      }

      toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }
  render() {
    if(this.state.newRule){
      return(
        <Redirect to={{
            pathname: '/render',
            state: {data: this.props.data, idForm: this.props.idForm, idRule: this.props.rules.id }
          }} 
        />
      )
    }
    return (
          <MDBCol  onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})}  className={"  radius  " +this.state.shadow }   size="3">
          <br/>
          <MDBIcon onClick={()=>this.viewRule()}  onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})} icon="eye" style={{color: '#00695c'}}  size="2x" className=" cursor"></MDBIcon>
        <div onClick={()=> this.setState({newRule: true})}>
          <br/>
            <img onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})}  className={"border radius  "}  src={require('../../img/rule.png')} width="200"/><br/><br/>
            <CardText onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})} size="2" className={"border radius  " +this.state.shadow }  id={this.props.rules.id} fs={true} color="#00695c" text={' version: ['+this.props.rules.versionRule+']'}/>  
            <label   ><b>creada el:</b> {this.props.rules.createdAt.split('T')[0] } - {this.props.rules.createdAt.split('T')[1].substring(0, 5) }  </label>
        </div>

        {/**
          ==============================================MODAL IMPORT RULE==========================
        */}        
          <MDBContainer>
                <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
                  <MDBModalHeader toggle={this.toggle}>Regla version no. {this.props.rules.versionRule}</MDBModalHeader>
                  <MDBModalBody>
                    <textarea disabled className="form-control" rows="4">
                    {this.state.rules}
                    </textarea>
                  </MDBModalBody>
                </MDBModal>
              </MDBContainer>
            </MDBCol>
    );
  }
}

export default RulesListBox;