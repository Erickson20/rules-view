import React, { Component } from 'react';
import { MDBRow, MDBCol,MDBIcon,MDBCard,  } from "mdbreact";
import {  Redirect } from 'react-router-dom';

import RenderRules from './renderRules';


class BtnNewRule extends Component {


    constructor(props){
        super(props)
        this.state = {
            shadow: '',
            newRule: false
        }
    }

    toggle(){
      this.setState({
        newRule: true
      })
    }
  render() {


    if(this.state.newRule){
      return(
        <Redirect to={{
                      pathname: '/render',
                      state: {data: this.props.data, idForm: this.props.idForm }
                     }} 
        />
      )
    }

    return (
      <div className="App padding">
      
                <MDBCol  onClick={()=>this.toggle()} onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})} size="12" className={"border radius padding " +this.state.shadow }>
                    <MDBIcon  onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})} icon="plus-circle" style={{color: '#00695c'}}  size="2x" className=" cursor"></MDBIcon>
                    <h5  onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})}>Nueva Regla</h5>
                </MDBCol>
       
      </div>
    );
  }
}

export default BtnNewRule;