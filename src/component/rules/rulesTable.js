import React, { Component } from 'react';
import { MDBRow, MDBCol,MDBAlert,MDBTable, MDBTableBody, MDBTableHead   } from "mdbreact";
import {GetRulesById} from "../../data/api";
import {  Redirect } from 'react-router-dom';
class RulesTable extends Component {

    constructor(props){
        super(props)
    
    }
    render() {
      console.log('this.props :', this.props);
        return (
        
        <MDBTable>
        <MDBTableHead color="default-color" textWhite>
          <tr>
            <th>ID</th>
            <th>ID DE FORMULARIO</th>
            <th>SERVICIO</th>
            <th>INSTITUCION</th>
            <th>ACCION</th>
          </tr>
        </MDBTableHead>
        <MDBTableBody>
        {this.props.data.length>0?
          this.props.data.map((forms, i)=>(
            <tr key={i}>
            <td><b>{forms.id}</b></td>
            <td><b>{forms.idForm}</b></td>
            <td><b>{forms.service_name}</b></td>
            <td><b>{forms.institution_name}</b></td>
            <td><button className="btn btn-primary btn-sm">ver reglas</button></td>
          </tr>
          ))
          :
          (<tr><td colspan="5"><h4>No hay formularios</h4></td></tr>)
        }
        </MDBTableBody>
      </MDBTable>
        
        );
    }
}
export default RulesTable;