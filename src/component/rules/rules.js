import React, { Component } from 'react';
import { MDBRow,MDBDataTable, MDBCol,MDBIcon,MDBCard,MDBProgress,MDBAlert,MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter,MDBBtn  } from "mdbreact";
import {GetForms,GetFormularyBySE, GetAllFormularyDataTable, GetAllMaskVersionsPerForm} from "../../data/api";
import RulesTable from './rulesTable';    
import {fieldSelected, allFieldSelected, DelteSelected} from '../../Helpers/helpers';
import {  Redirect } from 'react-router-dom';
import {  toast } from 'react-toastify';
import RulesList from './rulesList';

import './rules.css'
class Rules extends Component {

  constructor(props){
    super(props)
    this.state = {
      idForm: '',
      progress: 0,
      message: false,
      data:[],
      text: '',
      idFormSE: '',
      formExist: true,
      AllForm: [],
      loading: false
      
    }
    
    this.seachForm = this.seachForm.bind(this);
    this.idForm = this.idForm.bind(this);
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }
  async seachForm(idSE,id){
    this.setState({
      progress: 60
    })
    const data = await GetForms(idSE)
    if(data.ok){
          this.setState({
            progress: 80
          })
          if(data.data.hasOwnProperty('message')){
            this.setState({
              progress: 0,
              message: true,
              text: 'El formulario tiene errores o no existe'
            })
          
          }
          const result = await this.GetFormsById(idSE);
          console.log('result ok :', result);
          this.setState({
            progress: 100,
            data:   await this.fields(data.data),
            formExist : result.ok,
            idFormSE: id  
          })
     }else{
          this.setState({
            progress: 0,
            message: true,
            text: 'El id del formulario no existe'
        })
    }
  
  }
  idForm(e){
    this.setState({
      idForm: e.target.value
    })
  }

async AllmaskVersions(id){
  const maskData = await GetAllMaskVersionsPerForm(id);
  console.log(maskData.data);
   if(maskData.data.mask.length ===0){
    console.log('test')
    await toast.error('Este formulario no tiene version de Máscaras', {
      position: toast.POSITION.BOTTOM_CENTER
    })
  } else {
    this.toggle();
  }
  this.setState({
    maskStatus: await maskData.data.mask
  })
  let element = ''
    try{
      for(let i = 0 ; i < this.state.maskStatus.length; i++){
        let array = [];
        let version = await this.state.maskStatus[i]['maskVersion'];
        let mask = await this.state.maskStatus[i]['fieldsMasked'].split("D")
        for(let n = 0 ; n <mask.length; n++){
          if(mask[n].length < 3 && mask[n]!==''){
          array.push('Mask No.' + mask[n] +'=>')
          } else{
            array.push(mask[n]+'\n')
          }
          // let i = n -1
        }
        console.log(array);
        element = document.getElementById("status").value+='||'+'Version :'+version+'||'+'\n'+array+'\n'
        }
      }catch(e){
          console.log(e)
        }
      }
  async GetFormsById(id){
    const result = await GetFormularyBySE(id)
    return await result;
  }

  componentDidCatch(){
        this.AllmaskVersions();
  }
  async componentDidMount(){
    this.setState({
      loading: true
    })
    const data = await GetAllFormularyDataTable();
    let array = []
    try{
        for (const i of data.data.Formulary) {
          let element = {
            id: i.id,
            idForm: i.idForm,
            service: i.service_name,
            institution: i.institution_name,
            Reglas: <button  className="btn  btn-md btn-primary text-white" onClick={ e =>this.seachForm (i.idForm, i.id)}>Ver reglas</button>,
            Máscara: <button  className="btn  btn-md btn-primary text-white" onClick={ e =>this.AllmaskVersions(i.id)}>Ver Máscara</button>
          }
          array.push(element)
        }
      }catch(e){console.log(e)}
    const column = [
      {label:'id',field:'id',sort:'asc'},
      {label:'id del formulario',field:'id del formulario',sort:'asc'},
      {label:'Servicio',field:'servicio',sort:'asc'},
      {label:'Institutcion',field:'institutcion',sort:'asc'},
      {label:'Réglas',field:'accion',sort:'asc'},
      {label:'Máscara',field:'mask',sort:'asc'}
    ]
    const result = {columns:column, rows: array}
    this.setState({
      AllForm: result,
      loading: false
    })

  }

  fields(data){
    let fieldFrom = []
    let field = [];
    let page = []
    //console.log('result :', data);
    for (const key in data) {
      
        for (const i in data[key]) {
            if(data[key][i].type ==='radio-group' || data[key][i].type === 'select' || data[key][i].type === 'checkbox-group' ){
              fieldFrom.push(data[key][i])
            }
            field.push(data[key][i])
        }
        page[key] = [field,fieldFrom]
    }
    
    return page
}
  render() {
    
    if(!this.state.formExist){
      return  <Redirect to={{
        pathname: "/Formulary",
        state:{
          formExist: false,
          idForm: this.state.idForm
        }
      }}/>
    }
    return (
      
      this.state.data.length>0? (
          <RulesList formulary={this.state.idFormSE} data={this.state.data}/>
      ):
      (
        <div>
          <br/><br/><br/>
          <h1>Formularios para reglas</h1>
          <br/>
          <MDBRow>
          <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="lg" >
                  <MDBModalHeader toggle={this.toggle}>
                    <center>Versiones de Mascaras</center>
                  </MDBModalHeader>
                  <MDBModalBody>
                  <div>
                    <center>
                    <div className="form-group">
                      <textarea
                      id="status"
                      readOnly
                      className="form-control"
                      rows="5"
                      />
                    </div>
                    </center>
                  </div>
                  </MDBModalBody>
                  <MDBModalFooter>
                  <MDBBtn  color="primary"  onClick={()=> this.toggle()}>Salir</MDBBtn>
                  </MDBModalFooter>
            </MDBModal>
          <MDBCol size="1"></MDBCol>
          <MDBCol size="10">
          <MDBProgress value={this.state.progress} color="default" className="my-1 text-white white"  />
          {
            this.state.message && (
              <h4 className="text-danger"><b>{this.state.text}</b></h4>
            )
          }
          {/* <ToastContainer/> */}
          <MDBDataTable
            striped
            bordered
            hover
            data={this.state.AllForm}
          />
          {this.state.loading&&(
            <MDBIcon icon="cog" spin size="3x" style={{color: '#00695c'}} fixed />
        )}
          </MDBCol>
          <MDBCol size="1"></MDBCol>
          </MDBRow>
      
        </div>
      ) 
    );
          }
    
}
export default Rules;