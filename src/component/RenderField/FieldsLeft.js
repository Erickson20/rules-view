import React, { Component } from 'react';
import {MDBRow,MDBCard,MDBCol} from "mdbreact";
import {
    FCheckbox,
    FRadio,
    FSelect,
 } from "../form/index";
class FieldsLeft extends Component {
  constructor(props){
    super(props)
    this.state = {
      selected: true,
      class: 'padding ',
      rule: this.props.rule,
      Resumen:  [] 
    }
  }

  
  render() {
    const FromField = this.props.field
    //console.log('this.props.field :', this.props.field);
    return (
        
        <MDBRow >
        {FromField.map((field, i)=>(

                field.type === 'radio-group' ? (
                    <MDBCol size="6"  className="cursor">
                        <FRadio key={i} id={i + field.label} handlerFromInput={this.props.FromInput} fieldFromInput={this.props.FieldFromInput}  field={field}/>
                    </MDBCol>
                )
                :
                (field.type === 'select' && !field.select_portal_type) ? (
                    <MDBCol size="6" className="cursor" >
                        <FSelect key={i} id={i + field.label} handlerFromInput={this.props.FromInput} fieldFromInput={this.props.FieldFromInput} field={field}/>
                    </MDBCol>
                )

                :
                field.type === 'checkbox-group' && (
                    <MDBCol size="6" className="cursor" >
                        <FCheckbox key={i} id={i + field.label} handlerFromInput={this.props.FromInput} fieldFromInput={this.props.FieldFromInput} field={field}/>
                    </MDBCol>
                )
                    
            
        ))}
    </MDBRow>
    );
  }

 


 
}

export default FieldsLeft;