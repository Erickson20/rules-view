import React, { Component } from 'react';
import {MDBRow,MDBCard,MDBCol} from "mdbreact";
import { TFile,
    THeaders,
    TRadio,
    TSelect,
    TSubTitle,
    TText,
    TTextarea,
    TTime,
    TDate,
    TGrid,
    TCheckbox } from "../form/index";
class FieldsRight extends Component {
  constructor(props){
    super(props)
    this.state = {
      selected: true,
      class: 'padding ',
      rule: this.props.rule,
      Resumen:  [] 
    }
  }

  
  render() {
    const ToField = this.props.field
    console.log('this.props.field :', this.props.field);
    return (
        <MDBRow >
        {ToField.map((field, i)=>(
            field.type === 'text' ? (
                    <MDBCol size="6" className="cursor"  onClick={(e) => this.props.ToInput(e,field.name)}>
                        <TText key={i} id={i + field.label} fieldToInput={this.props.FieldToInput} field={field}/>
                    </MDBCol>
            )
            :
            field.type === 'radio-group' ? (
                <MDBCol size="6" className="cursor">
                <TRadio key={i} fieldToInput={this.props.FieldToInput}  handlerToInput={ this.props.ToInput} id={i + field.label} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'select'   ? (
                <MDBCol size="6" className="cursor" onClick={(e) => this.props.ToInput(e,field.name)}>
                <TSelect key={i} id={i + field.label} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'file' ? (
                <MDBCol size="6" className="cursor" onClick={(e) => this.props.ToInput(e,field.name)}>
                <TFile key={i} id={i + field.label} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'header' ? (
                
                
                <MDBCol size="12" className="cursor" onClick={(e) => this.props.ToInput(e,field.name)}>
                <hr/>
                <THeaders key={i} id={i + field.label}  handlerToInput={ this.props.ToInput} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'textarea' ? (
                <MDBCol size="6" className="cursor"  onClick={(e) => this.props.ToInput(e,field.name)}>
                    <TTextarea key={i} id={i + field.label} fieldToInput={this.props.FieldToInput} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'time' ? (
                <MDBCol size="6" className="cursor"  onClick={(e) => this.props.ToInput(e,field.name)}>
                    <TTime key={i} id={i + field.label} fieldToInput={this.props.FieldToInput} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'date' ? (
                <MDBCol size="6" className="cursor"  onClick={(e) => this.props.ToInput(e,field.name)}>
                    <TDate key={i} id={i + field.label} fieldToInput={this.props.FieldToInput} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'grid' ? (
                <MDBCol size="12" className="cursor"  >
                    <TGrid key={i} id={field.name} ToInput={this.props.ToInput} fieldToInput={this.props.FieldToInput} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'subtitle' ? (
                <MDBCol size="12" className="cursor" onClick={(e) => this.props.ToInput(e,field.name)}>
                <TSubTitle key={i} id={i + field.label} field={field}/>
                </MDBCol>
            )
            :
            field.type === 'checkbox-group' && (
                <MDBCol size="6" className="cursor" >
                <TCheckbox key={i} id={i + field.label} fieldToInput={this.props.FieldToInput}  handlerToInput={ this.props.ToInput}  field={field}/>
                </MDBCol>
                )
            
            
        ))}
    </MDBRow>
    );
  }

 


 
}

export default FieldsRight;