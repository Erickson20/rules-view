import React, { Component } from 'react';
import CardText from '../Utilities/CardText';
import { MDBRow, MDBCol,MDBIcon,MDBCard,MDBProgress,MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter  } from "mdbreact";
import ReactJson from 'react-json-view'
import { CreateRuleRadioButton, CreateRuleSelect, fieldSelected, allFieldSelected,createMask} from '../../Helpers/helpers';
import { validateForm,sendForm,GetRulesByForm, GetRulesById, GetLastversionFormByService, GetForms ,GetAllMaskVersionsPerForm, GetMaskVersionById} from "../../data/api";
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
class cardForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            modal: false,
            modalJSON: false,
            message: '',
            error: false,
            loading: false,
            loadingForRule: false,
            shadow: '',
            arrayRule: [],
            arrayMask:[],
            arrayWithMaskParams:[],
            json: '',
            submit: true,
            lastVersion: this.props.form.versionForm,
            jsonBySE: '',
            loadingData: false,
            maskChange: false,
            rulesChange: false,
            modalError: false,
            validate: false,
            errorSE: []
        }
    }
    toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }

      toggleError = () => {
        this.setState({
          modalError: !this.state.modalError
        });
      }
    toggleJSON = () => {
        this.setState({
          modalJSON: !this.state.modalJSON
        });
      }

     async saveForm(){
       this.setState({
         loading: true
       })
      
         let body = {
            name : this.props.form.idForm,
            fields: this.state.json,
            service_id: this.props.form.idservice,
            institution_id: this.props.form.idinstitution,
            version: this.state.lastVersion,
            entity: this.props.form.entity
         }
         console.log('body :', body);
          const result = await sendForm(body)
          console.log('result! :', result);
         
          if(result.ok){
            const lastVersion = await GetLastversionFormByService(this.props.form.idservice);
            this.setState({
              message: result.data.message,
              error:  false,
              modal: false,
              loading:false,
              lastVersion: lastVersion.data.version?lastVersion.data.version:1
            })
            toast.success('Se envio Correctamente el formulario: '+ this.props.form.idForm, {
              position: toast.POSITION.BOTTOM_CENTER
            });
          }else{
            this.setState({
              message: result.data.message,
              error:  true,
              loading:false,
              modalError: true
            })
            toast.error('Error al enviar el formulario!', {
              position: toast.POSITION.BOTTOM_CENTER
            });
          }
      }
      fields(data, rule){
        for (const i in data) {
            for (const key in data[i]) {
                if(data[i][key].type ==='radio-group' ){
                  CreateRuleRadioButton(data,i,key,rule) 
                }
                if(data[i][key].type ==='select' ){
                  CreateRuleSelect(data,i,key,rule) 
                }
            }
         }
        return data;
    }
    convertRule(result){
      let array = []
      let arrayRule = result.data.rules[0].rule.split('&');
      for (const i in arrayRule) {
          const element = arrayRule[i].split('=')[0];
          array[element]= arrayRule[i]
      }
      return array;
    }
    async setMaskVersion(e){

      if( e.target.value == '0'){
        this.setState({
          maskChange: false
        })
        return
      }

      if( e.target.value == 'noMask' && this.state.jsonBySE !== undefined ){
        this.state.jsonBySE[0].forEach(function(value,i){
          value['Mask'] =""
        });
        this.setState({
          maskChange: true,
          json:this.state.jsonBySE
        })
        return
      }
      const resultMask = await GetMaskVersionById(e.target.value);
      // console.log(resultMask.data.mask['fieldsMasked'])
        createMask(resultMask);


    this.setState({
      maskChange: true,
    })

  } 
   async SaveJson(e){
    if( e.target.value == '0'){
      this.setState({
        rulesChange: false,
      })
      return
    }
    if( e.target.value == 'noRule'){
      this.setState({
        rulesChange: true,
        json:this.state.jsonBySE
      })
      return
    }
      //await this.sleep(1000);
      const jsonRule = [];
      const resultRule = await GetRulesById(e.target.value);
      const rule = this.convertRule(resultRule);
     for (const key in rule) {
       jsonRule.push(rule[key]);   
     }
     let data = await this.fields(this.state.jsonBySE, rule)
    //  console.log(this.state.arrayMask, 'Veamos esto 2 porfavor');
     this.setState({
       json: data,
       rulesChange: true
     });
    //  for(let i = 0; i < data.length; i++){
    //   console.log(data[0][0], 'Veamos esto');
    //  }
     console.log(data)
   }

    async validate() {

      this.setState({
        loading: true,
        maskChange: false,
        validate: true
      })
      let fail = []
      let arrayError = []
      let service =  this.props.form.service_name.split("-")
      let  body = {
        fields: this.state.json,
        entity:  this.props.form.entity,
        process_id : service[0].trim()
      }
      const result = await validateForm(body)
      console.log(result)
      for (const key in result.data[0]) {
        if(result.data[0][key].Code !== "1"){
          fail.push(result.data[0][key]);
        }
      }
      console.log('result :', fail);
      if(fail.length>0){
        for (const key in fail) {
            arrayError.push({nombre: fail[key].Campo.Nombre,
                            id: fail[key].Campo.id,
                            value: fail[key].Campo.sentValue,
                            detail: fail[key].Detail
                            })
        }
        
        this.setState({
          validate: false,
          modalError: true,
          errorSE:arrayError
        })
      }else{
        this.setState({
          validate: true,
          modalError: false,
          errorSE:arrayError
        })
        toast.success('El formulario funciona correctamente', {
          position: toast.POSITION.BOTTOM_CENTER
        });
      }
      this.setState({
        loading: false,
        maskChange: true
      })
      console.log('this.state :', this.state);
    }

      async componentDidMount(){
        this.setState({
          loadingData: true
        })
        
        const form = await GetForms(this.props.form.idForm)
        const result = await GetRulesByForm(this.props.form.id)
        const lastVersion = await GetLastversionFormByService(this.props.form.idservice);
        const maskVersions =  await GetAllMaskVersionsPerForm(this.props.form.id);
        this.setState({
          arrayRule: result.data.rules,
          lastVersion: lastVersion.data.version?lastVersion.data.version:1,
          arrayMask:maskVersions.data.mask,
          jsonBySE: form.data && form.data,
          loadingData: false
        })
      }
      async componentWillReceiveProps(props){
        if(props !== this.props){
        this.setState({
          loadingData: true
        })
        const form = await GetForms(props.form.idForm)
        const result = await GetRulesByForm(props.form.id)
        const lastVersion = await GetLastversionFormByService(props.form.idservice);
        const maskVersions =  await GetAllMaskVersionsPerForm(props.form.id);
       
        this.setState({
          arrayRule: result.data.rules,
          lastVersion: lastVersion.data.version?lastVersion.data.version:1,
          arrayMask:maskVersions.data.mask,
          jsonBySE: form.data,
          loadingData: false
        })
        }
        
      }
      sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
        render() {
          let { ad } = this.props;
          return (
            <div className="App padding">
            {/* <ToastContainer/> */}
              <div onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})} size="2" className={"border radius padding " +this.state.shadow } onClick={this.toggle}>
            
              <label>{this.props.form.institution_name?this.props.form.institution_name.substring(0,26):'no se encontro la institucion'}...</label>
                  <br/>
                      <img onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})}  className={"border radius padding " +this.state.shadow } src={require('../../img/test.png')} width="200"/><br/><br/>
                      <label onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})}  className={"border radius padding " +this.state.shadow }><b>{this.props.form.service_name?this.props.form.service_name.split('-')[0]:'no se encontro servicio'}</b> || {this.props.form.idservice}</label>
                      <CardText onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-2'})}  className={"border radius padding " +this.state.shadow }  id={this.props.form.idForm} fs={true} color="#00695c" text={this.props.form.idForm}/>  
                      
              </div>
              {/** ===========================MODAL===================================*/}
              <MDBContainer>
              <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="lg" >
                  <MDBModalHeader toggle={this.toggle}>Formulario: {this.props.form.idForm}</MDBModalHeader>
                  
                  {this.state.loadingData?(
                    <div>
                      <MDBIcon icon="cog" spin size="2x" style={{color: '#00695c'}} fixed />
                      <label>cargando....</label>
                    </div>
                    )
                    :
                    (
                      <MDBModalBody>
                  <p className="h4 text-center mb-4">Enviar Formulario al Portal</p>
                  <MDBRow>
                  <MDBCol>
                  <br />
                      <label htmlFor="defaultFormLoginEmailEx" >
                      Version del formulario
                      </label>
                      <input
                      type="number"
                      id="id_service"
                      name="id_service"
                      className="form-control"
                      onChange={ this.idService }
                      value={ this.state.lastVersion}
                      disabled />
                  </MDBCol>
                  <MDBCol>
                    <br/>
                                <label htmlFor="defaultFormLoginEmailEx" >
                                Id formulario
                                </label>
                                <input
                                type="text"
                                id="id_formulary"
                                name="id_formulary"
                                className="form-control"
                                onChange={ this.idForm }
                                value={ this.props.form.idForm }
                                disabled />
                                <br />
                  </MDBCol>
                </MDBRow>
                                <label htmlFor="defaultFormLoginEmailEx" >
                                 servicio
                                </label>
                                <input
                                type="text"
                                id="id_service"
                                name="id_service"
                                className="form-control"
                                onChange={ this.idService }
                                value={ this.props.form.service_name}
                                disabled />
                            <br />
                                <label htmlFor="defaultFormLoginEmailEx" >
                                 institucion
                                </label>
                                <input
                                type="text"
                                id="id_institution"
                                name="id_institution"
                                className="form-control"
                                onChange={ this.idInsti }
                                value={this.props.form.institution_name }
                                disabled />
                           
                            <MDBRow>
                            <br />
                            <MDBCol>
                              <br/>
                            <label htmlFor="defaultFormLoginEmailEx" >
                              Version de Máscara
                            </label>
                            <select onChange={(e)=> this.setMaskVersion(e)} className="browser-default custom-select">
                                <option value="0">Seleccione un version de Máscara</option>
                                  {this.state.arrayMask.length >0 &&
                                    this.state.arrayMask.map((masks,i)=>
                                    <option key={i} value={masks.id}>Version:{
                                    masks.maskVersion} || {masks.createdAt && masks.createdAt.split('T')[0]} 
                                    {masks.createdAt.split('T')[1].substring(0, 5)
                                     }</option>
                                    )
                                  }
                                  <option value="noMask">Enviar sin máscara</option>
                            </select>
                            </MDBCol>
                            <MDBCol>
                            <br />
                                <label htmlFor="defaultFormLoginEmailEx" >
                                Version de Regla
                                </label>
                                <select onChange={(e)=> this.SaveJson(e)} className="browser-default custom-select">
                                <option value="0">Seleccione un version</option>
                                  {typeof this.state.arrayRule !== 'undefined'&&this.state.arrayRule.length>0&&
                                    this.state.arrayRule.map((rules,i)=>
                                      <option key={i} value={rules.id}>Version: {rules.versionRule} ||
                                       {rules.createdAt&& rules.createdAt.split('T')[0]} 
                                       {rules.createdAt.split('T')[1].substring(0, 5) }
                                       </option>
                                    )
                                  }
                                  
                                <option value="noRule">Enviar sin reglas</option>
                                
                                </select>
                                {this.state.loadingForRule&&(
                                  <div>
                                    <MDBIcon icon="cog" spin size="2x" style={{color: '#00695c'}} fixed />
                                    <label>Asignando regla....</label>
                                  </div>
                                  )}
                            </MDBCol>
                            </MDBRow>

                              <br/>
                </MDBModalBody>
                    )
                  }
                 
                  <MDBModalFooter>
                    {this.state.error&&(
                        <center><label className="red-text">{this.state.message}</label></center>
                    )}
                    {this.state.loading&&(
                      <MDBIcon icon="cog" spin size="3x" style={{color: '#00695c'}} fixed />
                    )}
                  <MDBBtn hidden={ this.state.validate? true: false}   outline color="default" disabled={this.state.maskChange && this.state.rulesChange? false: true} onClick={()=> this.validate()}>Validar Formulario</MDBBtn>
                  <MDBBtn color="default" disabled={this.state.maskChange && this.state.rulesChange && this.state.validate? false: true} onClick={()=> this.saveForm()}>Enviar al portal</MDBBtn>
                  </MDBModalFooter>
              </MDBModal>
              </MDBContainer>
              {/** ===========================MODAL ERROR===================================*/}
              <MDBContainer>
                  <MDBModal isOpen={this.state.modalError} toggle={this.toggleError} side position="bottom-right">
                  <MDBModalHeader toggle={this.toggleError}>Errores del formulario</MDBModalHeader>
                  <MDBModalBody>
                    {this.state.errorSE.length>0&&
                      this.state.errorSE.map((field,i)=>(
                        <div>
                          <label>{field.nombre +' - '+field.id}</label><br/>
                          <label>Valor enviado: {field.value}</label><br/>
                          <label>Detalle: {field.detail}</label><br/>
                          <hr/>
                        </div>
                    ))}
                  </MDBModalBody>
                  <MDBModalFooter>
                    
                  </MDBModalFooter>
                </MDBModal> 
              </MDBContainer>

             
            </div>
          );
        }
  }
export default cardForm;