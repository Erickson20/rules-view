import React, { Component } from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdbreact';
class Formulary extends Component {
    constructor(props){
        super(props)
    }
    sendForm(event){
        event.preventDefault();
        const form = event.target
        const data = new FormData(event.target);
        console.log(form);
    }
  render() {
      console.log('***** STATE1',this.state);
    return (
      <div className="App">
      <br/><br/><br/>
       <MDBContainer >
        <MDBRow>
            <MDBCol size="3"></MDBCol>
            <MDBCol size="6">
            <MDBRow className="card">
                    <MDBCol md="12">
                    <form onSubmit={this.sendForm}>
                        <p className="h4 text-center mb-4">Enviar Formulario al Portal</p>
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Id formulario
                        </label>
                        <input
                        type="text"
                        id="id_formulary"
                        name="id_formulary"
                        className="form-control"
                        
                        />
                        <br />
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Id servicio
                        </label>
                        <input
                        type="text"
                        id="id_service"
                        name="id_service"
                        className="form-control"
                        />
                        <br />
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Id institucion
                        </label>
                        <input
                        type="text"
                        id="id_institution"
                        name="id_institution"
                        className="form-control"
                        />
                        <br />
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Entorno
                        </label>
                        <select id="id_env" name="id_env" className="browser-default custom-select">
                        <option>Seleccione</option>
                        <option value="1">DESARROLLO</option>
                        <option value="2">PRUEBA</option>
                        <option value="3">PRE-PRODUCCION</option>
                        <option value="4">PRODUCCION</option>
                        </select>
                        <br /><br />
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Version
                        </label>
                        <input
                        type="text"
                        id="version"
                        name="version"
                        className="form-control"
                        />
                        <br />
                    
                        <div className="text-center mt-4">
                        <MDBBtn color="default" type="submit">Login</MDBBtn>
                        </div>
                    </form>
                    </MDBCol>
                </MDBRow>
            </MDBCol>
            <MDBCol size="3"></MDBCol>
        </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}

export default Formulary;