import React, { Component } from 'react';
import CardText from '../Utilities/CardText';
import { MDBRow, MDBCol,MDBIcon,MDBCard,MDBProgress  } from "mdbreact";
import CardForm from './cardForm';
import NewForm from './btnNewForm';
import { MDBTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { GetAllFormulary,seachFormulary } from "../../data/api";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {env,api} from "../../config";
class AllFormulary extends Component {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            formExist: false,
            message: false,
            statusConnection: api,
            gettingErrors:false
        }
        this.dataForm = this.dataForm.bind(this);
    }
    async componentDidMount(){
        // console.log(this.props, 'aqui')
        // console.log(this.state.statusConnection,'observando');
        // if(this.state.statusConnection !== 'http://localhost:4200/rules/api'){
        //     this.setState({statusConnection:'error'})
        // }
        const datagetter = await GetAllFormulary();
        console.log(datagetter.ok)
        if(datagetter.ok){
            this.setState({
                gettingErrors:true
            })
        }
        try{
        if(datagetter.data.Formulary.length>0){
            this.setState({
                data: datagetter.data.Formulary
            })
        } 
    } catch(e){
        console.log(e);
        }
    }
    async dataForm(e){
        try{
        if(this.state.data.length>0){
            const datagetter = await GetAllFormulary();
            if( datagetter.data.Formulary.length==0){
                this.setState({
                    message: true
                    })
            }
            this.setState({
                data: datagetter.data.Formulary
            })
        }
    }catch(e){return false}
    }
    async seach(e){
        console.log('seach :', e.target.value);
        try{
            const seach = await seachFormulary(e.target.value);
        if( seach.data.Formulary.length==0){
            this.setState({
                message: true
                })
        }
        this.setState({
            data: seach.data.Formulary
            })
        }catch(e){
            return false
        } 
    }
    componentWillMount(){
        if(this.props.location.state){
            toast.error('Tienes que crear el formulario ['+this.props.location.state.idForm+'] antes de crear sus reglas', {
                position: toast.POSITION.TOP_CENTER
              });
        }
    }
  render() {
     return (
     <div className="App padding">
        {/* <ToastContainer/> */}
         <NewForm handlerData={this.dataForm} />
        <input type="text" placeholder="buscar formulario" className="form-control" name="seach" onChange={e=> e.target.value? this.seach(e) : this.dataForm(e)}/><br/>
        {(this.state.statusConnection !== undefined && this.state.data.length == 0 && this.state.statusConnection !== 'error' && this.state.message===false )
        && ( <MDBIcon icon="cog" spin size="4x" style={{color: '#00695c'}} fixed /> )}    
        {(this.state.data.length==0 && this.state.message===true)&&( <h3>no existen formularios</h3>)}    
        {this.state.data.length>0&& 
        <MDBRow>
        {this.state.data.map((form,i) =>(
            <MDBCol key={i} size="3" className="padding">
                <CardForm form={form}/>
            </MDBCol>
        ) )}
        </MDBRow>
       }
     </div>
    );
  }
}
export default AllFormulary;