import React, { Component } from 'react';
import CardText from '../Utilities/CardText';
import { MDBRow, MDBCol,MDBIcon,MDBCard,MDBProgress ,MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter   } from "mdbreact";
import {GetForms, GetFormularyByServices,saveForm, GetServicesByInstituion, GetLastAllInstitutions,GetLastversionFormByService} from "../../data/api";
import { ToastContainer, toast } from 'react-toastify';
import Select from 'react-select';
class NewForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            modal: false,
            shadow: '',
            idForm: '',
            idService: '0',
            idInsti: '0',
            error: '',
            imgUrl:'',
            loading: false,
            institutions: [],
            services: [],
            selectedOptionInstitution: null,
            selectedOptionService: null,
            modalError: false,
            errorSE: {}
        }
        this.idForm = this.idForm.bind(this);
        this.idInsti = this.idInsti.bind(this);
        this.idService = this.idService.bind(this);
    }    
    async componentDidMount(){
        this.setState({
          imgUrl:"https://html.com/wp-content/uploads/flamingo.jpg"
        })
        const institutions = await GetLastAllInstitutions();
        console.log('institutions :', institutions);
        const options = [];
        try{
        for (const i in institutions.data) {
            options.push( { value: institutions.data[i].id, label: institutions.data[i].id +'|'+ institutions.data[i].name });
        }
      }catch(e){
      }
        console.log('options :', options);
        this.setState({
            institutions: options
        })
    }
    toggleError = () => {
      this.setState({
        modalError: !this.state.modalError
      });
    }
    toggle = () => {
        this.setState({
          modal: !this.state.modal
        });
      }
      idForm(e){
        this.setState({
          idForm: e.target.value
        })
      }
      idService(e){
        this.setState({
          idService: e.target.value
        })
      }
      async idInsti(e){
          let id = await e.target.value || '0'
        const services = await GetServicesByInstituion(id)
        this.setState({
            idInsti: id,
            services: services.data
        })
      }
      handleChange = async selectedOptionInstitution => {
        this.setState({ selectedOptionInstitution });
        const services = await GetServicesByInstituion(selectedOptionInstitution.value)
        const options = [];
        for (const i in services.data) {
            options.push( { value: services.data[i].id, label: services.data[i].name +'('+ services.data[i].process+')' });
        }
        this.setState({
            idInsti: selectedOptionInstitution.value,
            services: options
        })
      };
      handleChangeService = async selectedOptionService => {
        this.setState({
          idService: selectedOptionService.value,
            selectedOptionService
        })
      };
       async saveForm(){
        this.setState({
            loading: true
        })
        const form =  await GetForms(this.state.idForm)
        if(form.ok){
              if(this.state.idInsti=='' || this.state.idService=='' || this.state.imgUrl==''){
                  this.setState({error: 'todos los campos tienen que estar llenos'})
              }else{
                const version = await GetLastversionFormByService(this.state.idService);
                const servicesExist = await GetFormularyByServices(this.state.idService)
                console.log('servicesExist :', servicesExist);
                if(servicesExist.data.servicesExist){
                  this.setState({error: 'Este servicio ya esta registrado',loading: false})
                  return 
                }
                this.setState({error: ''})
                let body = {
                jsonForm:form.data,
                idForm: this.state.idForm,
                idservice: parseInt( this.state.idService),
                idinstitution: parseInt(this.state.idInsti),
                url:this.state.imgUrl,
                entity: form.entity,
                version: version.data.version
              }
              console.log('body :', body);
              const result = await saveForm(body);
              console.log('resulttito :', result);
                  if(result.data.form){
                      this.setState({modal: false})
                      this.props.handlerData()
                  }else{
                      this.setState({error: 'Hubo un error al guardar el formulario por favor revisar el log'})
                  }
              }
        }else{

          let result = {
            detail: form.data,
            error: form.error,
            field: form.field
          }
            this.setState({
                error: form.data,
                modalError: true,
                errorSE: result
                
            })
            toast.error("El formulario tiene errores!", {
              position: toast.POSITION.BOTTOM_CENTER
            });
          
          
        }
        this.setState({
            loading: false
        })
      }
    render() {
      console.log('this.state.errorSE :', this.state.errorSE);
        return (
        <div className="App padding">
        {/* <ToastContainer/> */}
        <MDBRow  onClick={this.toggle} >
                <MDBCol onMouseOut={()=> this.setState({shadow: ''})} onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})} size="2" className={"border radius padding " +this.state.shadow }>
                    <MDBIcon  onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})} icon="plus-circle" style={{color: '#00695c'}}  size="2x" className=" cursor"></MDBIcon>
                    <h5  onMouseEnter={()=> this.setState({shadow: 'z-depth-1'})}>Nuevo Formulario</h5>
                </MDBCol>
                <MDBCol size="5"></MDBCol>
                <MDBCol size="5">
                </MDBCol>
        </MDBRow>
        {/** ===========================MODAL===================================*/}
        <MDBContainer>
        <MDBModal isOpen={this.state.modal} toggle={this.toggle} size="lg" >
            <MDBModalHeader toggle={this.toggle}>Nuevo Formulario</MDBModalHeader>
            <MDBModalBody>
            <p className="h4 text-center mb-4">Busca un formulario en softexpert</p>
                        <label htmlFor="defaultFormLoginEmailEx" >
                        Id formulario
                        </label>
                        <input
                        type="text"
                        id="id_formulary"
                        name="id_formulary"
                        className="form-control"
                        onChange={ this.idForm }
                        value={ this.state.idForm }
                        autoComplete /> 
                        {this.state.loading&&(
                            <MDBIcon icon="cog" spin size="3x" style={{color: '#00695c'}} fixed />
                        )}
                        <br />
                        <MDBRow>
                        <MDBCol>
                        <label htmlFor="defaultFormLoginEmailEx" >
                           institucion
                             </label>
                             <Select
                             value={this.state.selectedOptionInstitution}
                             onChange={this.handleChange}
                             options={this.state.institutions}
                             placeholder="seleccione una institucion"
                              />
                             {this.state.loadingForRule&&(
                               <div>
                                 <MDBIcon icon="cog" spin size="2x" style={{color: '#00695c'}} fixed />
                                 <label>Asignando regla....</label>
                               </div>
                               )}
                        </MDBCol>
                        <MDBCol>
                             <label htmlFor="defaultFormLoginEmailEx" > Servicio </label>
                                  <Select
                                    value={this.state.selectedOptionService}
                                    onChange={this.handleChangeService}
                                    options={this.state.services}
                                    placeholder="seleccione una institucion"
                                      />
                                  {this.state.loadingForRule&&(
                                    <div>
                                      <MDBIcon icon="cog" spin size="2x" style={{color: '#00695c'}} fixed />
                                      <label>Asignando regla....</label>
                                    </div>
                                    )}
                        </MDBCol>
                        <MDBCol>
                             <label htmlFor="defaultFormLoginEmailEx" > Imagen de la Institución (URL) </label>
                             <input
                              type="text"
                              className="form-control"
                              value={ this.state.imgUrl }
                              autoComplete
                              />
                        </MDBCol>
                        </MDBRow>
                        <br />          
            </MDBModalBody>
            <MDBModalFooter>
            <center><label className="red-text">{this.state.error}</label></center>
            <MDBBtn color="default" disabled={this.state.idInsti ==='0' || this.state.idService ==='0'? true: false} onClick={()=> this.saveForm()}>Guardar</MDBBtn>
            </MDBModalFooter>
        </MDBModal>
        </MDBContainer>

        {/**==================================MODAL ERROR=============== */}
        <MDBModal isOpen={this.state.modalError} toggle={this.toggleError} side position="bottom-right">
        <MDBModalHeader toggle={this.toggleError}>{this.state.errorSE.error}</MDBModalHeader>
        <MDBModalBody>
          <label><b>Razon:</b> {this.state.errorSE.detail}</label><br/>
          <label><b>Campo:</b> {this.state.errorSE.field&&this.state.errorSE.field.label }</label><br/>
          <label><b>Id:</b> {this.state.errorSE.field&&this.state.errorSE.field.name}</label>

        </MDBModalBody>
      </MDBModal> 
    
        </div>
        );
    }
}
export default NewForm;