 function fieldSelected(id){
    let field  = ExistField(id);
    if(field){
        DelteSelected()
    field.setAttribute("style", "border-style: dotted;border-width: 2px; border-color:#16B9B3");
    }else{
        alert('Este campo tiene atributos null y no se puede tomar el id');
    }
  }
  function DelteSelected() {
    let oldField = document.getElementById('ToInput').value;
    oldField &&( oldField.indexOf(',') > -1?  allDelteSelected(oldField): ExistField(oldField).setAttribute("style", "") );
  }
  function allDelteSelected(fields) { 
    let arrayField = fields.split(',');
    for (let i = 0; i < arrayField.length; i++) {
        const id = arrayField[i];
        if(ExistField(id)){
            ExistField(id).setAttribute("style", "") 
        }
        
    }
  }
  function allFieldSelected(fields){
      let arrayField = fields.split(',');
      for (let i = 0; i < arrayField.length; i++) {
          const id = arrayField[i];
          if(ExistField(id)){
            ExistField(id).setAttribute("style", "border-style: dotted;border-width: 2px; border-color:#16B9B3")
          }else{
            alert('Este campo tiene atributos null y no se puede tomar el id')
          }
          
      }
  }

  function nameRule(rule){
    switch (rule) {
      case '0':
        return 'Ocultar'
      case '1':
        return 'Visualizar'
      case '2':
        return 'inhabilitado'
      case '3':
        return 'habilitado'
      case '4':
        return 'Requerido'
      case '5':
        return 'clickear'
      case '6':
        return 'No Requerido'
      case '7':
        return 'limpiar'
      case '8':
        return 'Cambiar a CEDULA'
      case '9':
        return 'Cambiar a PASAPORTE'
      case '10':
        return 'Cambiar a RNC'
      default:
        break;
    }
  }

  function ExistField(id) {
    let field = document.getElementById(id);
    if(field){
        return field
    }else{
        field = document.getElementsByName(id)[0];
        if(field){
            return field 
        }else{
            return null;
        }
    }
    
  }
  function CreateRuleRadioButton(data,i,key,rule){
    for (const idRule in rule) {    
      if (data[i][key].name === idRule.substr(1,idRule.length)) {
          for (const idValue in data[i][key].values) {
              if (data[i][key].values[idValue].value == idRule.substr(0,1)) {
                data[i][key].values[idValue].rule = rule[idRule].split('=')[1]
                //console.log('RADIO ID RULE',data[i][key].values[idValue]);
              }
          }
      }
    }
  }

  function  CreateRuleSelect(data,i,key,rule){
    for (const idRule in rule) {          
      if (data[i][key].name === idRule.split('|')[1]) {
          for (const idValue in data[i][key].values) {
              if (data[i][key].values[idValue].value == idRule.split('|')[0]) {
                data[i][key].values[idValue].rule = rule[idRule].split('=')[1]
                //console.log('SELECT ID RULE',data[i][key].values[idValue]);
              }
          }
      }
    }
  }
  function createMask(resultMask) {
    try{
      let res = resultMask.data.mask['fieldsMasked']; 
      let fieldsMaskedSetters = res.split("D");
          this.state.jsonBySE[0].forEach(function(value){
            if(value['type']==='text' || value['type']==='time' || value['type']==='date'){
              for (const key in fieldsMaskedSetters) {
                if(fieldsMaskedSetters[key] === value['name']){
                  let i = key - 1;
                  value['Mask'] = fieldsMaskedSetters[i];
                }
              }
            }
          })
  } catch (e) {return false}
    
  }
  function ErrorByFirebase(idError) {
    switch (idError) {
      case 'auth/email-already-in-use':
        return 'Ya existe este usuario'
      case 'auth/weak-password':
        return 'la contraseña tiene ser minimo de  6 caracteres'
      case 'auth/wrong-password':
        return 'la contraseña es incorrecta'
      case 'auth/too-many-requests':
        return 'Se estan haciendo muchos intentos sesion en esta cuenta'
      case 'auth/user-not-found':
        return 'No existe una cuenta con este usuario'
      default:
        break;
    }
    
  }
export   {
    fieldSelected,
    nameRule,
    allFieldSelected,
    DelteSelected,
    CreateRuleRadioButton,
    CreateRuleSelect,
    ErrorByFirebase,
    createMask
}