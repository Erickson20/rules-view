import React from "react";
import { Route, Switch } from "react-router-dom";

import App from "./component/app/App";
import Rules from "./component/rules/rules";
import Page404 from "./component/404/Page404";
import Home from "./component/Home/home";
import Mask from "./component/Mask/mask";
import AllFormulary from "./component/formulary/allFormulary";
import RenderRules from './component/rules/renderRules';
import Mails from './component/Mails/Main';

const AppRoutes = () =>
<App>
    <Switch>
        <Route path="/Rules" component={Rules}/>
        <Route path="/Mask" component={Mask}/>
        <Route path="/Formulary" component={AllFormulary}/>
        <Route path="/render" component={RenderRules}/>
        <Route path="/Mails" component={Mails}/>
        <Route path="/" component={Home}/>
        <Route  component={Page404}/>
    </Switch>
</App>
export default AppRoutes